const express = require("express");
const mysql  = require("mysql2");
const cors = require("cors");
const { get } = require("http");

const app = express();
app.use(express.json());
app.use(cors("*"));

//const database = require("../jenkins/db");

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database: 'dacdb'
  })

connection.connect(function(err){
    if(err) throw err
    console.log('You are now connected...')

})



app.get('/student',(req,res)=>{
    connection.query('SELECT * FROM student_tb',(err,rows)=>{
       if(err){
        console.log(err)
       } 
       else{
       
       res.send(rows)
       }
    })
})

 
app.delete('/student/:id',(req,res)=>{
    connection.query('DELETE FROM student_tb WHERE id=?',[req.params.id],(err,rows)=>{
       if(err){
        console.log(err);
       } 
       else{
         res.send(rows)
       }
    })
})

app.post('/student',(req,res)=>{
   var student= req.body
   var studentData=[student.id,student.name,student.password,student.course,student.passingyear,student.prnno,student.dob]
    connection.query('INSERT INTO student_tb(id,name,password,course,passingyear,prnno,dob)values(?)',[studentData],(err,rows)=>{
       if(err){
        console.log(err)
       } 
       else{
       
       res.send(rows)
       }
    })
})
app.put('/student',(req,res)=>{
    var student= req.body
     connection.query('UPDATE student_tb SET ? WHERE id='+student.id,[student],(err,rows)=>{
        if(err){ 
         console.log(err)
        } 
        else{
        if(rows.affectedRows==0){
            var studentData=[student.id,student.name,student.password,student.course,student.passingyear,student.prnno,student.dob]
            connection.query('INSERT INTO student_tb(id,name,password,course,passingyear,prnno,dob)values(?)',[studentData],(err,rows)=>{
               if(err){
                console.log(err)
               } 
               else{
               
               res.send(rows)
               }
            })
        }
        else{
            res.send(rows)
        }
        res.send(rows)
        }
     })
 })




app.listen(8050,()=>{
    console.log("server started on 8050");
})
